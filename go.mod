module gitea.com/macaron/i18n

go 1.11

require (
	gitea.com/macaron/macaron v1.5.1-0.20201027213641-0db5d4584804
	github.com/gopherjs/gopherjs v0.0.0-20190430165422-3e4dfb77656c // indirect
	github.com/stretchr/testify v1.4.0
	github.com/unknwon/i18n v0.0.0-20190805065654-5c6446a380b6
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897 // indirect
	golang.org/x/text v0.3.2
	gopkg.in/ini.v1 v1.62.0 // indirect
)
